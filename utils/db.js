require('dotenv').config()
const models = require('../models') 
const mongoose = require('mongoose')

async function inserUser(data) {
  const resData = new models.userModel(data)
  return resData.save()
}

async function findOne (collection, data) {
  return await mongoose.Collection(collection).findOne(data)
}

async function updateOne (collection, query, data) {
  return await mongoose.Collection(collection).updateOne(query, data)
}

module.exports = {
  findOne,
  mongoose,
  updateOne,
  inserUser
}