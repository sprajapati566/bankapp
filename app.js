require('dotenv').config()
const express = require('express')
const routes = require('./routes')
const db = require('./utils').mongoose

const PORT = process.env.PORT
const app = express()
const url = `${process.env.DB_URL}/${process.env.DB_NAME}`

db.connect(url, {useNewUrlParser: true}).then(()  => {
  console.log('Connected to the database')
}).catch(err => {
  console.log('Error connecting to database', err.message)
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use('/', routes.userRoutes)
app.use('/transaction', routes.tnxRoutes)

app.listen(PORT, () => {
  console.log(`Listening at ${PORT}`)
})