const db = require('../utils')

function electricity () {
  console.log('okay')
}

async function revertTnx (user, balance) {
  return await db.updateOne('users', {id: user.id}, {balance})
}

async function transfer (userData, body) {
  try {
    const toUser = await db.findOne('users', body.to)
    if (toUser) {
      const remainingBalance = userData.balance - body.amount
      const updateFromData = await db.update('users', {id: body.id}, {balance: remainingBalance})
      if(updateFromData.nModified === 1) {
        try {
          await db.updateOne('users', {id: toUser.id}, {balance: toUser.balance + body.amount}) 
          return ({success: true, data: {message: 'Transaction successful', balance: remainingBalance}})
        } catch(error) {
          await revertTnx(userData, userData.balance)
          return ({success: false, message: `${error.message}. Transaction reverted`})
        }
      } else {
        return ({success: false, message: 'failed to transfer money'})
      }
    } else {
      return ({ success: false, message: 'To user not found' })
    }
  } catch(error) {
    return ({ success: false, message: error.message })
  }
}

module.exports = {
  transfer,
  electricity
}
