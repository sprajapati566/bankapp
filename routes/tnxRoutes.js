const express = require('express')
const tnxController = require('../controller')

const route = express.Router()

route.post('/pay', tnxController.pay)

module.exports = route