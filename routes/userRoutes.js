const express = require('express')
const userController = require('../controller')

const route = express.Router()

route.post('/register', userController.register)

module.exports = route