const tnxRoutes = require('./tnxRoutes')
const userRoutes = require('./userRoutes')

module.exports = {
    tnxRoutes,
    userRoutes
}