const dbUtils = require('../utils')
const helpers = require('../helpers').tnxHelper

const paymentTypes = {
  transfer: '1',
  telephone: '2',
  mobile: '3',
  electricity: '4'
}

async function pay (req, res) {
  const body = req.body
  try {
    const userData = await dbUtils.findOne({id: body.id})
    if(userData) {
      if(userData.balance >= body.amount) {
        if(body.type === paymentTypes.transfer) await helpers.transfer(userData, body)
      } else {
        res.send({success: false, message: 'User does\'t have enough balance'})
      }
    } else {
      res.send({success: false, message: 'User not found'})
    }
  } catch (error) {
    res.send({success: false, message: error.message})
  }
}

module.exports = {
  pay
}