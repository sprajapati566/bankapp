const {
  register
} = require('./userController')

const {
  pay
} = require('./tnxController')

module.exports = {
  pay,
  register
}