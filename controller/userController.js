const userModel = require('../utils')

async function register(req, res) {
  const body = req.body
  try {
    const data = await userModel.insert(body)
    console.log(data)
    res.send({ success: true, data: { message: 'User registered', id: data.id} })
  } catch(error) {
    res.send({ success: false, message: error.message})
  }
}

module.exports = {
  register
}